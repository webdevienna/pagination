window.onload = function(){
    // Aktive Schaltfläche der Pagination hervorheben / Higlight the current pagination
    const activePage = window.location.pathname + window.location.search;
    const paginationLinks = document.querySelectorAll("a");
    paginationLinks.forEach(link => {
        if (link.href.includes(activePage) && link.search == window.location.search) {
            link.classList.add("pagination_active");
            console.log(link.search);
        }
    });
}