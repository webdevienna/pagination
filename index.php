<?php

    // Verbindung zur Datenbank / Connect to database
    $pdo = new PDO("mysql:host=localhost;dbname=db_dummy", "root", "");
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

    // Abfrage für die Anzahl der Einträge / Query for number of rows
    $stmt = $pdo->query("SELECT COUNT(*) FROM authors");
    $number_of_records = $stmt->fetchColumn();

    // Wieviele Einträge pro Seite sollen angezeigt werden / Set results per page
    $results_per_page = 10;

    // Berechnung wie viele Schaltflächen für die Pagination benötigt werden / Count how many button for pagination are needed
    // Aufrunden der Zahl auf die nächste Ganzzahl / Need "ceil()" to round up to the next integer
    $total_pages = ceil($number_of_records / $results_per_page);

    // Abfrage der aufgerufenen Seite / Check for page
    if (isset($_GET["page"]) ? $page = $_GET["page"] : $page = 0);

    // Abfrage welche Seite aufgerufen ist / Check for requested page
    if ($page > 1) {
        $start_page = ($page * $results_per_page) - $results_per_page;
    } else {
        $start_page = 0;
    }

    // Abfrage / Query
    $stmt = $pdo->query("SELECT * FROM authors ORDER BY last_name LIMIT $start_page, $results_per_page");
    $result = $stmt->fetchAll();
?>

<!DOCTYPE html>
<html lang=de">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./style.css">
    <script src="./main.js"></script>
    <title>PHP Pagination</title>

</head>
<body>
    <table>
        <thead>
            <th>ID</th>
            <th>FIRST NAME</th>
            <th>LAST NAME</th>
            <th>EMAIL</th>
            <th>BIRTHDAY</th>
            <th>ADDED</th>
        </thead>
        <tbody>
            <?php foreach ($result as $res) : ?>
                <tr>
                    <td><?= $res["id"]?></td>
                    <td><?= $res["first_name"]?></td>
                    <td><?= $res["last_name"]?></td>
                    <td><?= $res["email"]?></td>
                    <td><?= $res["birthdate"]?></td>
                    <td><?= $res["added"]?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
    <div class="pagination_wrapper">
        <a href="?page=1">&laquo;</a>
        <a href="?page=<?= ($page == 1) ? $page = 1 : $page - 1; ?>">&lsaquo;</a>

        <span class="pagination">
        <?php for ($pagination = 1; $pagination <= $total_pages; $pagination++) : ?>
            <a href="?page=<?= $pagination; ?>"><?= $pagination; ?></a>
            <?php endfor ?>
        </span>

        <a href="?page=<?= ($page == $total_pages) ? $page = $total_pages: $page + 1; ?>">&rsaquo;</a>
        <a href="?page=<?= $total_pages ?>">&raquo;</a>
    </div>
</body>
</html>